// template-related scripts go here... 

// https://gitlab.com/-/snippets/2071984
jQuery(window).load(function() { // web page has finished 

    function isMobile() {
    var index = navigator.appVersion.indexOf("Mobile"); // detect chrome for android and ios safari
    var index2 = navigator.appVersion.indexOf("Android"); // detect firefox for android
    if (index2) { return (index2 > -1); }
    return (index > -1);
    }   

    usingmobile = isMobile();
    if (usingmobile){
        jQuery('input').filter(function(){
        return !jQuery(this).attr('size');
        }).attr('size',''); // change to a number if desired
        jQuery('textarea').filter(function(){
        return !jQuery(this).attr('size');
        }).attr('cols',''); // change to a number if desired

        jQuery("#edit__summary").attr('size', '');
    }
       
});

